﻿using OpenWeatherMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace sensei_weather
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public bool IsReady = false;

        public string LoadedCity = "Tokyo";

        public CurrentWeatherResponse Weather;

        public MainWindow()
        {
            InitializeComponent();
            Loaded += OnWindowLoad;
        }

        public string UpdatedAt
        {
            get
            {
                return Weather.LastUpdate.Value.ToLocalTime().ToString();
            }
        }

        public string TemperatureParams
        {
            get
            {
                return (Weather.Temperature.Value - 273.15).ToString();
            }
        }

        public async void OnWindowLoad(object sender, RoutedEventArgs e)
        {
            DataContext = new
            {
                IsInitialized = this.IsReady
            };

            /**
             * Solved problem with System.Threading.Tasks -> Install-Package Microsoft.Bcl.Async
             * 
             * */
            Weather = await GetWeather(this.LoadedCity);
            IsReady = true;

            RefreshInterface();
        }

        public static async Task<CurrentWeatherResponse> GetWeather(string cityName)
        {
            var client = new OpenWeatherMapClient("b4f2676ba556892197df87db5464c8ba");
            var currentWeather = await client.CurrentWeather.GetByName(cityName);
            return currentWeather;
        }

        public async void RefreshWeather(object sender, RoutedEventArgs e)
        {
            IsReady = false;
            RefreshInterface();

            Weather = await GetWeather(this.LoadedCity);
            IsReady = true;
            RefreshInterface();
        }

        public async void LoadNewWeather(object sender, RoutedEventArgs e)
        {
            try
            {
                this.IsReady = false;
                RefreshInterface();
                this.LoadedCity = CityName.Text;
                Weather = await GetWeather(this.LoadedCity);
                this.IsReady = true;
                RefreshInterface();

            } catch (OpenWeatherMapException)
            {
                return;
            }
        }

        public void RefreshInterface()
        {
            var fullFilePath = $"http://openweathermap.org/img/w/{Weather.Weather.Icon}.png";

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(fullFilePath, UriKind.Absolute);
            bitmap.EndInit();

            WeatherIcon.Source = bitmap;

            DataContext = new
            {
                IsReady = this.IsReady,
                IsNotReady = !this.IsReady,
                Weather = this.Weather,
                UpdatedAt = this.UpdatedAt,
                TemperatureParams = this.TemperatureParams
            };
        }
    }
}
